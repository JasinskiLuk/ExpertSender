﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSender.Utilities
{
    public class HttpClientExtension
    {
        public static async Task<string> HttpGet(string entity, int? id = null)
        {
            using HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44334/api/");

            string parameter = entity;

            if (id != null)
                parameter += "/" + id;

            HttpResponseMessage response = await client.GetAsync(parameter);

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsStringAsync().Result;
            }
            else
            {
                string error;

                if (response.Content.ReadAsStringAsync().Result != string.Empty)
                    error = response.Content.ReadAsStringAsync().Result;
                else
                    error = response.ReasonPhrase;

                throw new Exception(error);
            }
        }

        public static async Task HttpCreate(object dto)
        {
            using HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44334/api/");

            string entity = dto.GetType().Name[..^3];
            string parameter = entity;
            string serializedDto = JsonConvert.SerializeObject(dto);
            StringContent stringContent = new StringContent(serializedDto, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(parameter, stringContent);

            if (!response.IsSuccessStatusCode)
            {
                string error;

                if (response.Content.ReadAsStringAsync().Result != string.Empty)
                    error = response.Content.ReadAsStringAsync().Result;
                else
                    error = response.ReasonPhrase;

                throw new Exception(error);
            }
        }

        public static async Task HttpUpdate(object dto)
        {
            using HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44334/api/");

            string entity = dto.GetType().Name[..^3];
            string parameter = entity;
            string serializedDto = JsonConvert.SerializeObject(dto);
            StringContent stringContent = new StringContent(serializedDto, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PutAsync(parameter, stringContent);

            if (!response.IsSuccessStatusCode)
            {
                string error;

                if (response.Content.ReadAsStringAsync().Result != string.Empty)
                    error = response.Content.ReadAsStringAsync().Result;
                else
                    error = response.ReasonPhrase;

                throw new Exception(error);
            }
        }

        public static async Task HttpDelete(string entity, int personId)
        {
            using HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44334/api/");

            string parameter = entity + "/" + personId;

            HttpResponseMessage response = await client.DeleteAsync(parameter);

            if (!response.IsSuccessStatusCode)
            {
                string error;

                if (response.Content.ReadAsStringAsync().Result != string.Empty)
                    error = response.Content.ReadAsStringAsync().Result;
                else
                    error = response.ReasonPhrase;

                throw new Exception(error);
            }
        }
    }
}
