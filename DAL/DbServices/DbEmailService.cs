﻿using EFTesting.DTOs;
using EFTesting.IServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFTesting.DbServices
{
    public class DbEmailService : IEmailService
    {
        private readonly EFTestingContext _context;

        public DbEmailService(EFTestingContext context)
        {
            _context = context;
        }

        public async Task<EmailDTO> Get(int emailId)
        {
            EmailDTO email = null;

            try
            {
                email = await _context.Emails
                    .FirstOrDefaultAsync(f => f.Id == emailId);

                email.Person = await GetPersonForEmail(email.PersonId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return email ?? new NullEmailDTO();
        }

        public async Task<IEnumerable<EmailDTO>> Get()
        {
            IEnumerable<EmailDTO> emails = Enumerable.Empty<EmailDTO>();

            try
            {
                emails = await _context.Emails
                    .ToListAsync();

                foreach (EmailDTO email in emails)
                        email.Person = await GetPersonForEmail(email.PersonId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return emails;
        }

        private async Task<PersonDTO> GetPersonForEmail(int? personId)
        {
            PersonDTO person = null;

            try
            {
                person = await _context.Persons
                            .OrderByDescending(f => f.Id)
                            .FirstOrDefaultAsync(f => f.Id == personId);
            }
            catch (Exception)
            {
                throw;
            }

            return person ?? new NullPersonDTO();
        }

        public async Task<int> Create(EmailDTO email)
        {
            try
            {
                _context.Add(email);
                await _context.SaveChangesAsync();

                return email.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Update(EmailDTO email)
        {
            try
            {
                _context.Entry(email).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Delete(int emailId)
        {
            try
            {
                EntityEntry<EmailDTO> email = _context.Emails.Attach(new EmailDTO { Id = emailId });
                email.State = EntityState.Deleted;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> CheckIfExists(int emailId)
        {
            try
            {
                return await _context.Emails.AnyAsync(email => email.Id == emailId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
