﻿using EFTesting.DTOs;
using EFTesting.IServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFTesting.DbServices
{
    public class DbPersonService : IPersonService
    {
        private readonly EFTestingContext _context;

        public DbPersonService(EFTestingContext context)
        {
            _context = context;
        }

        public async Task<PersonDTO> Get(int personId)
        {
            PersonDTO person = null;

            try
            {
                person = await _context.Persons
                    .FirstOrDefaultAsync(p => p.Id == personId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return person ?? new NullPersonDTO();
        }

        public async Task<IEnumerable<PersonDTO>> Get()
        {
            IEnumerable<PersonDTO> persons = Enumerable.Empty<PersonDTO>();

            try
            {
                persons = await _context.Persons
                    .ToListAsync();

                foreach (PersonDTO person in persons)
                {
                    IEnumerable<EmailDTO> emails = await _context.Emails
                            .Where(p => p.PersonId == person.Id)
                            .ToListAsync();
                    person.Emails = emails;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return persons;
        }

        public async Task<int> Create(PersonDTO person)
        {
            try
            {
                _context.Persons.Add(person);
                await _context.SaveChangesAsync();

                return person.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Update(PersonDTO person)
        {
            try
            {
                _context.Entry(person).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Delete(int personId)
        {
            try
            {
                EntityEntry<PersonDTO> person = _context.Persons.Attach(new PersonDTO { Id = personId });
                person.State = EntityState.Deleted;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> CheckIfExists(int personId)
        {
            try
            {
                return await _context.Persons.AnyAsync(person => person.Id == personId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> CheckIfCanBeDeleted(int personId)
        {
            try
            {
                return !await _context.Emails.AnyAsync(i => i.Id == personId);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
