﻿using EFTesting.DTOs;
using Microsoft.EntityFrameworkCore;

namespace EFTesting.DbServices
{
    public class EFTestingContext : DbContext
    {
        public EFTestingContext(DbContextOptions<EFTestingContext> options) : base(options) { }

        public DbSet<PersonDTO> Persons { get; set; }
        public DbSet<EmailDTO> Emails { get; set; }
    }
}
