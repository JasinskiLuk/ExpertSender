﻿using EFTesting.Interfaces;
using EFTesting.DTOs;

namespace EFTesting.IServices
{
    public interface IEmailService : IReadService<EmailDTO>, IReadCollectionService<EmailDTO>, ICreateUpdateService<EmailDTO>, IDeleteService
    {
    }
}
