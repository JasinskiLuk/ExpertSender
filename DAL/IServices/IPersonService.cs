﻿using EFTesting.Interfaces;
using EFTesting.DTOs;
using System.Threading.Tasks;

namespace EFTesting.IServices
{
    public interface IPersonService : IReadService<PersonDTO>, IReadCollectionService<PersonDTO>, ICreateUpdateService<PersonDTO>, IDeleteService
    {
        Task<bool> CheckIfCanBeDeleted(int Id);
    }
}
