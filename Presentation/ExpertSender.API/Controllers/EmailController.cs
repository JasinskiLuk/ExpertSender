﻿using ExpertSender.DTOs;
using ExpertSender.IServices;
using ExpertSender.Utilities;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExpertSender.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private ILogger _log => Log.ForContext<EmailController>();
        private readonly IEmailService _emailService;

        public EmailController(IEmailService emailService)
        {
            _emailService = emailService;
        }

        /// <summary>
        /// Get email list
        /// </summary>
        /// <response code="200">Email</response>
        /// <response code="404">Email list is empty</response>
        /// <response code="500">Server error</response>
        [HttpGet]
        public async Task<IActionResult> GetEmails()
        {
            IEnumerable<EmailDTO> emails = Enumerable.Empty<EmailDTO>();

            try
            {
                emails = await _emailService.Get();

                if (!emails.Any())
                {
                    _log.Here().Information("Email list is empty");
                    return NotFound("Email list is empty");
                }
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return Ok(emails);
        }

        /// <summary>
        /// Get email
        /// </summary>
        /// <response code="200">Email</response>
        /// <response code="404">Email with specified Id not found</response>
        /// <response code="500">Server error</response>
        [HttpGet("{emailId}")]
        public async Task<IActionResult> GetEmail(int emailId)
        {
            EmailDTO email = null;

            try
            {
                email = await _emailService.Get(emailId);

                if (email is NullEmailDTO)
                {
                    _log.Here().Information("Email with the specified Id not found");
                    return NotFound("Email with the specified Id not found");
                }
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return Ok(email);
        }

        /// <summary>
        /// Create email
        /// </summary>
        /// <response code="201">Email created</response>
        /// <response code="500">Server error</response>
        [HttpPost]
        public async Task<IActionResult> CreateEmail(EmailDTO email)
        {
            int emailId = -1;

            try
            {
                emailId = await _emailService.Create(email);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return CreatedAtAction(nameof(GetEmail), new { emailId }, email);
        }

        /// <summary>
        /// Update email
        /// </summary>
        /// <response code="204">Email updated</response>
        /// <response code="404">Can not update email. Person not exists</response>
        /// <response code="500">Server error</response>
        [HttpPut]
        public async Task<IActionResult> UpdateEmail(EmailDTO email)
        {
            try
            {
                bool exists = await _emailService.CheckIfExists(email.Id);

                if (!exists)
                {
                    _log.Here().Information("Can not update email. Person not exists");
                    return NotFound("Can not update email. Person not exists");
                }

                await _emailService.Update(email);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return NoContent();
        }

        /// <summary>
        /// Delete email
        /// </summary>
        /// <response code="204">Email deleted</response>
        /// <response code="404">Can not delete email. Person not exists</response>
        /// <response code="500">Server error</response>
        [HttpDelete("{emailId}")]
        public async Task<IActionResult> DeleteEmail(int emailId)
        {
            try
            {
                bool exists = await _emailService.CheckIfExists(emailId);

                if (!exists)
                {
                    _log.Here().Information("Can not delete email. Person not exists");
                    return NotFound("Can not delete email. Person not exists");
                }

                await _emailService.Delete(emailId);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return NoContent();
        }
    }
}
