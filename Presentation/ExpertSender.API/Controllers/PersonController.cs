﻿using ExpertSender.DTOs;
using ExpertSender.IServices;
using ExpertSender.Utilities;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExpertSender.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private ILogger _log => Log.ForContext<PersonController>();
        private readonly IPersonService _personService;

        public PersonController(IPersonService personService)
        {
            _personService = personService;
        }

        /// <summary>
        /// Get person list
        /// </summary>
        /// <response code="200">Person list</response>
        /// <response code="404">Person list is empty</response>
        /// <response code="500">Server error</response>
        [HttpGet]
        public async Task<IActionResult> GetPersons()
        {
            IEnumerable<PersonDTO> persons = Enumerable.Empty<PersonDTO>();

            try
            {
                persons = await _personService.Get();

                if (!persons.Any())
                {
                    _log.Here().Information("Person list is empty");
                    return NotFound("Person list is empty");
                }
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return Ok(persons);
        }

        /// <summary>
        /// Get person
        /// </summary>
        /// <response code="200">Person</response>
        /// <response code="404">Person with specified Id not found</response>
        /// <response code="500">Server error</response>
        [HttpGet("{personId}")]
        public async Task<IActionResult> GetPerson(int personId)
        {
            PersonDTO person = null;

            try
            {
                person = await _personService.Get(personId);

                if (person is NullPersonDTO)
                {
                    _log.Here().Information("Person with specified Id not found");
                    return NotFound("Person with specified Id not found");
                }
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return Ok(person);
        }

        /// <summary>
        /// Create person
        /// </summary>
        /// <response code="201">Person created</response>
        /// <response code="500">Server error</response>
        [HttpPost]
        public async Task<IActionResult> CreatePerson(PersonDTO person)
        {
            int personId = -1;

            try
            {
                personId = await _personService.Create(person);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return CreatedAtAction(nameof(GetPerson), new { personId }, person);
        }

        /// <summary>
        /// Update person
        /// </summary>
        /// <response code="204">Person updated</response>
        /// <response code="404">Can not update person. Person not exists</response>
        /// <response code="500">Server error</response>
        [HttpPut]
        public async Task<IActionResult> UpdatePerson(PersonDTO person)
        {
            try
            {
                bool exists = await _personService.CheckIfExists(person.Id);

                if (!exists)
                {
                    _log.Here().Information("Can not update person. Person not exists");
                    return NotFound("Can not update person. Person not exists");
                }

                await _personService.Update(person);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return NoContent();
        }

        /// <summary>
        /// Delete person
        /// </summary>
        /// <response code="204">Person deleted</response>
        /// <response code="404">Can not delete person. Person not exists</response>
        /// <response code="409">Can not delete person. Person is in use</response>
        /// <response code="500">Server error</response>
        [HttpDelete("{personId}")]
        public async Task<IActionResult> DeletePerson(int personId)
        {
            try
            {
                bool exists = await _personService.CheckIfExists(personId);

                if (!exists)
                {
                    _log.Here().Information("Can not delete person. Person not exists");
                    return NotFound("Can not delete person. Person not exists");
                }

                bool canBeDeleted = await _personService.CheckIfCanBeDeleted(personId);

                if (!canBeDeleted)
                {
                    _log.Here().Information("Can not delete person. Person is in use");
                    return Conflict("Can not delete person. Person is in use");
                }

                await _personService.Delete(personId);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return NoContent();
        }
    }
}
