﻿using ExpertSender.DTOs;
using ExpertSender.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExpertSender.WebApp.Controllers
{
    public class PersonController : Controller
    {
        private ILogger _log => Log.ForContext<PersonController>();

        public async Task<IActionResult> Index()
        {
            IEnumerable<PersonDTO> persons = Enumerable.Empty<PersonDTO>();

            try
            {
                if (TempData["error"] != null)
                    ModelState.AddModelError(string.Empty, TempData["error"].ToString());

                string result = await HttpClientExtension.HttpGet("person");
                persons = JsonConvert.DeserializeObject<IEnumerable<PersonDTO>>(result);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                ModelState.AddModelError(string.Empty, ex.GetBaseException().Message);
            }

            return View(persons);
        }

        public async Task<IActionResult> BasicForm(int? personId)
        {
            PersonDTO person = null;

            try
            {
                if (personId != null)
                {
                    string personResult = await HttpClientExtension.HttpGet("person", personId);
                    person = JsonConvert.DeserializeObject<PersonDTO>(personResult);
                }
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                ModelState.AddModelError(string.Empty, ex.GetBaseException().Message);
            }

            return View(person);
        }

        [HttpPost]
        public async Task<IActionResult> Create(PersonDTO person)
        {
            try
            {
                await HttpClientExtension.HttpCreate(person);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                TempData["error"] = ex.GetBaseException().Message;
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Update(PersonDTO person)
        {
            try
            {
                await HttpClientExtension.HttpUpdate(person);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                TempData["error"] = ex.GetBaseException().Message;
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Delete(int personId)
        {
            try
            {
                await HttpClientExtension.HttpDelete("person", personId);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                TempData["error"] = ex.GetBaseException().Message;
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
