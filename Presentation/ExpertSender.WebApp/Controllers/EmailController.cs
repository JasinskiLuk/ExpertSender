﻿using ExpertSender.DTOs;
using ExpertSender.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExpertSender.WebApp.Controllers
{
    public class EmailController : Controller
    {
        private ILogger _log => Log.ForContext<EmailController>();

        public async Task<IActionResult> Index()
        {
            IEnumerable<EmailDTO> emails = Enumerable.Empty<EmailDTO>();

            try
            {
                if (TempData["error"] != null)
                    ModelState.AddModelError(string.Empty, TempData["error"].ToString());

                string result = await HttpClientExtension.HttpGet("email");
                emails = JsonConvert.DeserializeObject<IEnumerable<EmailDTO>>(result);

                string resultPersons = await HttpClientExtension.HttpGet("person");
                IEnumerable<PersonDTO> persons = JsonConvert.DeserializeObject<IEnumerable<PersonDTO>>(resultPersons);

                if (!persons.Any())
                {
                    _log.Here().Information("Person list is empty");
                    throw new Exception("Person list is empty");
                }

                ViewBag.Person = persons.LastOrDefault();
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                ModelState.AddModelError(string.Empty, ex.GetBaseException().Message);
            }

            return View(emails);
        }

        public async Task<IActionResult> BasicForm(int? emailId)
        {
            EmailDTO email = null;
            IEnumerable<PersonDTO> persons = Enumerable.Empty<PersonDTO>();

            try
            {
                if (emailId != null)
                {
                    string resultEmail = await HttpClientExtension.HttpGet("email", emailId);
                    email = JsonConvert.DeserializeObject<EmailDTO>(resultEmail);
                }

                string resultPersons = await HttpClientExtension.HttpGet("person");
                persons = JsonConvert.DeserializeObject<IEnumerable<PersonDTO>>(resultPersons);

                if (!persons.Any())
                {
                    _log.Here().Information("Person list is empty");
                    throw new Exception("Person list is empty");
                }

                ViewBag.Persons = new SelectList(persons, "Id", "FullName");
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                ModelState.AddModelError(string.Empty, ex.GetBaseException().Message);
            }

            return View(email);
        }

        [HttpPost]
        public async Task<IActionResult> Create(EmailDTO email)
        {
            try
            {
                await HttpClientExtension.HttpCreate(email);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                TempData["error"] = ex.GetBaseException().Message;
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Update(EmailDTO email)
        {
            try
            {
                await HttpClientExtension.HttpUpdate(email);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                TempData["error"] = ex.GetBaseException().Message;
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Delete(int emailId)
        {
            try
            {
                await HttpClientExtension.HttpDelete("email", emailId);
            }
            catch (Exception ex)
            {
                _log.Here().Error(ex.GetBaseException().Message);
                TempData["error"] = ex.GetBaseException().Message;
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
