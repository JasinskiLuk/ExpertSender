﻿using System.Threading.Tasks;

namespace EFTesting.Interfaces
{
    public interface ICreateUpdateService<T>
    {
        Task<int> Create(T model);
        Task Update(T model);
    }
}
