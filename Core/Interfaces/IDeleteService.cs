﻿using System.Threading.Tasks;

namespace EFTesting.Interfaces
{
    public interface IDeleteService
    {
        Task Delete(int Id);
    }
}
