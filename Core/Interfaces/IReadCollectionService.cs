﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace EFTesting.Interfaces
{
    public interface IReadCollectionService<T>
    {
        Task<IEnumerable<T>> Get();
    }
}
