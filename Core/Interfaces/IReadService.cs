﻿using System.Threading.Tasks;

namespace EFTesting.Interfaces
{
    public interface IReadService<T>
    {
        Task<T> Get(int Id);
        Task<bool> CheckIfExists(int Id);
    }
}
