﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFTesting.DTOs
{
    public class EmailDTO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int Id { get; set; }
        [MaxLength(50)]
        public string EmailAddress { get; set; }
        public PersonDTO Person { get; set; }
        public int? PersonId { get; set; }
    }

    public class NullEmailDTO : EmailDTO
    {
        public NullEmailDTO()
        {
            Id = -1;
            EmailAddress = "Empty";
        }
    }
}
