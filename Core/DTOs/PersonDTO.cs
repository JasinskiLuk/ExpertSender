﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFTesting.DTOs
{
    public class PersonDTO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int Id { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }
        public string Description { get; set; }
        public IEnumerable<EmailDTO> Emails { get; set; }
    }

    public class NullPersonDTO : PersonDTO
    {
        public NullPersonDTO()
        {
            Id = -1;
            FirstName = "Empty";
            LastName = "Empty";
            Description = "Empty";
        }
    }
}
